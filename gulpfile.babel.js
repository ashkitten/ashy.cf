import autoprefixer from "gulp-autoprefixer";
import babel from "gulp-babel";
import browserSyncModule from "browser-sync";
import cleanCSS from "gulp-clean-css";
import data from "gulp-data";
import del from "del";
import frontMatter from "front-matter";
import gulp from "gulp";
import marked from "gulp-marked";
import moment from "moment";
import path from "path";
import plumber from "gulp-plumber";
import pug from "gulp-pug";
import reloadModule from "require-reload";
import sass from "gulp-sass";
import tap from "gulp-tap";
import toArray from "stream-to-array";
import uglify from "gulp-uglify";
import util from "gulp-util";
import wrap from "gulp-wrap";

const browserSync = browserSyncModule.create();
const reload = reloadModule(require);

const paths = (() => {
    var dest = `${__dirname}/public`;

    return {
        static: {
            src: `${__dirname}/static/**/*`,
            dest: `${dest}`
        },
        fonts: {
            src: [
                `${__dirname}/node_modules/font-awesome/fonts/fontawesome-webfont.*`,
                `${__dirname}/node_modules/material-design-icons/iconfont/MaterialIcons-Regular.*`
            ],
            dest: `${dest}/fonts`
        },
        pages: {
            src: `${__dirname}/src/pug/pages/**/*.pug`,
            dest: `${dest}`
        },
        posts: {
            src: `${__dirname}/content/posts/**/*.md`,
            dest: `${dest}/blog/posts`
        },
        projects: {
            src: `${__dirname}/content/projects/**/*.md`
        },
        scripts: {
            src: [
                `${__dirname}/node_modules/jquery/dist/jquery.min.js`,
                `${__dirname}/node_modules/jquery-validation/dist/jquery.validate.min.js`,
                `${__dirname}/node_modules/jquery.dotdotdot/dist/jquery.dotdotdot.js`,
                `${__dirname}/node_modules/popper.js/dist/umd/popper.min.js`,
                `${__dirname}/node_modules/bootstrap/dist/js/bootstrap.min.js`,
                `${__dirname}/node_modules/moment/min/moment.min.js`,
                `${__dirname}/src/js/**/*.js`
            ],
            dest: `${dest}/js`
        },
        styles: {
            src: [
                `${__dirname}/src/sass/**/*.scss`
            ],
            dest: `${dest}/css`
        }
    };
})();

function buildStatic() {
    return gulp.src(paths.static.src)
        .pipe(plumber())
        .pipe(gulp.dest(paths.static.dest));
}

function buildFonts() {
    return gulp.src(paths.fonts.src)
        .pipe(plumber())
        .pipe(gulp.dest(paths.fonts.dest));
}

function buildPages() {
    return gulp.src(paths.pages.src)
        .pipe(plumber())
        .pipe(data((file, callback) => {
            Promise.all([
                toArray(gulp.src(paths.posts.src)
                    .pipe(plumber())
                    .pipe(data((file) => {
                        var matter = frontMatter(String(file.contents));
                        file.contents = new Buffer(matter.body);
                        return matter.attributes;
                    }))
                    .pipe(marked())),
                toArray(gulp.src(paths.projects.src)
                    .pipe(plumber())
                    .pipe(data((file) => {
                        var matter = frontMatter(String(file.contents));
                        file.contents = new Buffer(matter.body);
                        return matter.attributes;
                    }))
                    .pipe(marked()))
            ])
            .then((result) => {
                callback(undefined,
                    Object.assign(
                        reload("./src/pug/defaults.json"),
                        {
                            moment: moment,
                            posts: result[0],
                            projects: result[1]
                        }
                    )
                );
            });
        }))
        .pipe(pug())
        .pipe(gulp.dest(paths.pages.dest));
}

function buildPosts() {
    return gulp.src(paths.posts.src)
        .pipe(plumber())
        .pipe(data((file) => {
            var matter = frontMatter(String(file.contents));
            file.contents = new Buffer(matter.body);
            return matter.attributes;
        }))
        .pipe(marked())
        .pipe(wrap(
            { src: "./src/pug/layout/post.pug" },
            (file) => {
                return Object.assign(
                    reload("./src/pug/defaults.json"),
                    file.data,
                    {
                        filename: "./src/pug/layout/post.pug",
                        moment: moment
                    }
                );
            },
            { engine: "pug" }
        ))
        .pipe(gulp.dest(paths.posts.dest));
}

function buildScripts() {
    return gulp.src(paths.scripts.src)
        .pipe(plumber())
        .pipe(tap((file, t) => {
            if(file.base == `${__dirname}/src/js/`) {
                return t.through(babel, []);
            }
        }))
        .pipe(uglify())
        .pipe(gulp.dest(paths.scripts.dest));
}

function buildStyles() {
    return gulp.src(paths.styles.src)
        .pipe(plumber())
        .pipe(sass({ includePaths: ["./node_modules"] }))
        .pipe(cleanCSS())
        .pipe(autoprefixer(["> 1%"]))
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(browserSync.stream());
}

function reloadBrowser(done) {
    browserSync.reload();
    done();
}

export const clean = () => del("./public");

export const build = gulp.series(clean,
    gulp.parallel(buildStatic, buildFonts, buildPages, buildPosts, buildScripts, buildStyles));

export function watch(done) {
    gulp.watch(paths.static.src, gulp.series(buildStatic, reloadBrowser));
    gulp.watch(["./src/pug/**/*", "./content/**/*"],
        gulp.series(gulp.parallel(buildPages, buildPosts), reloadBrowser));
    gulp.watch(paths.scripts.src, gulp.series(buildScripts, reloadBrowser));
    gulp.watch(paths.styles.src, buildStyles);
    done();
}

export const serve = gulp.series(build, watch, (done) => {
    browserSync.init({
        open: false,
        server: {
            baseDir: "./public",
            serveStaticOptions: {
                extensions: ["html"]
            }
        }
    });

    done();
});

export default serve;
