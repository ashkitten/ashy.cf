---
title: Brainf*ck Interpreter
short: A Brainf*ck interpreter written in pure x86 assembly
link: https://gitlab.com/ashkitten/bf-interpreter-nasm
image: /images/bf-interpreter.jpg
---

Brainfuck is an esoteric programming language designed for extreme minimalism.
Its programs consist of eight distinct characters which perform operations on
an array in memory. It was created by Urban Müller in 1992 for the Amiga family
of personal computers with the goal of having the smallest compiler possible.
In fact, the compiler he wrote has an executable size of just 240 bytes!
