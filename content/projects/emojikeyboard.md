---
title: Emoji Keyboard
short: An emoji selector written in Qt
link: https://gitlab.com/ashkitten/emojikeyboard-qt5
image: /images/emojikeyboard.jpg
---
