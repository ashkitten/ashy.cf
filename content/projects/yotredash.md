---
title: Yotredash
short: A shader demotool written in Rust
link: https://gitlab.com/ashkitten/yotredash
image: /images/yotredash.jpg
---

Yotredash is a shader demotool - it displays GLSL shaders in real-time
by rendering them on an OpenGL quad. It can be used to create very cool
artistic effects, and will work with many of the demos listed on
https://shadertoy.com (with some adaptation).
