$("form").validate({
    errorClass: "is-invalid",
    errorPlacement: (error, element) => {
        $(element)
            .next(".invalid-feedback")
            .html(error.text());
    }
});

$("form :input").on("input focusout", (event) => {
    $(event.currentTarget).valid();
    return true;
});

$(".dotdotdot").dotdotdot();

$('[data-toggle="more"][data-target]').click((event) => {
    var target = $($(event.currentTarget).attr("data-target"));
    target.toggleClass("show");
    target.addClass("showing");
    target.attr("aria-expanded", (!(target.attr("aria-expanded") == "true")).toString());
    return false;
});

$(".more").on("transitionend", (event) => {
    $(".more").removeClass("showing");
});

function updateDates() {
    $(".date[date]").each((i, element) => {
        $(element).text(`Posted ${moment($(element).attr("date")).fromNow()}`);
    });
}

updateDates();
setInterval(updateDates, 1000 * 60);
